package org.wltea.analyzer.dic;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.logging.log4j.Logger;
import org.wltea.analyzer.help.ESPluginLoggerFactory;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * 　　* @description: TODO
 * 　　* @param
 * 　　* @return
 * 　　* @throws
 * 　　* @author 陈宇
 * 　　* @date $ $
 *
 */
public class HotDictReloadThread  implements Runnable{

    private static final Logger logger = ESPluginLoggerFactory.getLogger(HotDictReloadThread.class.getName());

    @Override
    public void run() {
        while(true) {
            logger.info("[==========]reload hot dict from mysql......");
            //暂停十秒
            try {
                Thread.sleep(20000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            logger.info("开始进入调用方法");
            Dictionary.getSingleton().reLoadMainDict();
        }
    }

    public static void main(String[] args) {
        URL url = null;
        try {
            url = new URL("http://39.108.89.51:8916/esHotWords/getAllHotWords");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        try {
        InputStream is = url.openStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String line = null;
            while ((line = br.readLine()) != null) {
                JSONArray objects = JSONObject.parseArray(line);
                System.out.println("返回数据条数：" + objects.size());
                for (int i = 0; i < objects.size(); i++) {
                    if(!CharUtil.isChinese(objects.get(i).toString())){
                        System.out.println(objects.get(i).toString());
                    }

                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
